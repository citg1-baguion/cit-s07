-- MARC ANGELO BAGUION
-- S07 - ACTIVITY
/*

2. Create SQL Syntax and Queries based on the ERD.
-FROM Previous Activity S06


*/
--DB Name: blog_db


	CREATE DATABASE blog_db;

	USE blog_db;

	CREATE TABLE users(
		id INT NOT NULL AUTO_INCREMENT,
		email VARCHAR(100) NOT NULL,
		password VARCHAR(300) NOT NULL,
		datetime_created DATETIME,
		PRIMARY KEY(id)
	);

	CREATE TABLE posts(
		id INT NOT NULL AUTO_INCREMENT,
		author_id INT NOT NULL,
		title VARCHAR(500),
		content VARCHAR(5000),
		datetime_posted DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_posts_author_id
			FOREIGN KEY(author_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

	CREATE TABLE post_likes(
		id INT NOT NULL AUTO_INCREMENT,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		datetime_liked DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_post_likes_post
			FOREIGN KEY(post_id)
			REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT fk_post_likes_user
			FOREIGN KEY(user_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

	CREATE TABLE post_comments(
		id INT NOT NULL AUTO_INCREMENT,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		content VARCHAR(5000),
		datetime_commented DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_post_comments_post
			FOREIGN KEY(post_id)
			REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT fk_post_comments_user
			FOREIGN KEY(user_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);
----CURRENT ACTIVITY S07
------------------------------------------------------------------------
-- 3A. [USERS] 
-- Add the records to users table to the blog_db database (previous activity)
INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
	("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
	("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
	("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
	("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- 3A. [Posts]
-- Add the records to posts table to the blog_db database (previous activity)
INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
	(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
	(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:0,0"),
	(4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

--4. Get all the POST with an author ID of 1.
SELECT * FROM posts WHERE author_id = 1;

--5.  Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;

--6.  Update a post's content to Hello to the people of the Earth! where its initial content is Hello Earth! by using the record's ID
UPDATE posts SET content = "Hello to the people of the Earth!"
WHERE id = (SELECT id FROM posts WHERE content = "Hello Earth!");

--7.  Delete the user with an email of johndoe@gmail.com.
DELETE FROM users WHERE email = "johndoe@gmail.com";
